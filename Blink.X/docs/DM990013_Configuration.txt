DM990013 - CEC1x02 Development Board Configuration
--------------------------------------------------

Jumper List:
------------
X5 - IN
X7 - IN
JP2 - <2-3>

JTAG Debug Hdr: J2

SPI FLASH Programming Hdr: PIM.J2 using mikroProg for CEC

R32 on prototype boards has 10K pull-up which need to change to 0 ohm in case some tools may have connection issue such as Segger J-Link Plus