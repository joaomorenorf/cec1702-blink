/*****************************************************************************
* � 2017 Microchip Technology Inc. and its subsidiaries.
* You may use this software and any derivatives exclusively with
* Microchip products.
* THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".
* NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
* INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY,
* AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP
* PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.
* IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
* INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
* WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
* BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.
* TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL
* CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF
* FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
* MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE
* OF THESE TERMS.
*****************************************************************************/
/** @file  CEC1x02_main.c
 ****************************************************************************
 * CEC1x02_main.c
 * 
 * Initialize CEC1x02 Development board timer/keyboard interrupts to: 
 *      -blink LED LD3 
  *      CEC1x02 MotherBoard settings:
 *      X1 < >
 *      X2 < >
 *      X3 < >
 *      X4 < >
 *      X5 < >
 *      X6 < >
 *      X7 <IN>
 *      X8 < >
 *      X9 < >
 *      J2 <PICkit4 Debugger>
 *      J3 <OPEN>
 *      JP1 < >
 *      JP2 <2-3>
 *      J4 (LCD ribbon)
 *      J5 <1-2>
 *      J12 (LCD Backlight)
 *      X7 <IN>
 *
 *      CEC1702 PIM settings:
 *      T1 <OUT>
 *      J2 <ALL OUT>
 **************************************************************************** 
 */
//#include <stdio.h>
                                     // define MMCR and macros
#include <xc.h>  
#include <stdlib.h>
#include <string.h>

#include "interrupt.h"
#include "gpio.h"
 
#define TIMER_PRE_SCL_DIV_48    0x2F0000U	                        //Hex value Divider for 47
#define DIRECT_NVIC		1                                           //direct NVIC parameter

extern void uart_init(void);
extern void print_str(char *);

void TIMER16_0_Handler(void);
void board_init(void);
void timer_init(void);
void timer_delay_1ms(uint32_t);				

volatile uint32_t TIMER0_TIMEOUT = 0;							
volatile uint32_t ISR_COUNT = 0;
volatile uint8_t T_DONE = 0;

/**  main
* @note This is the main module for CEC1x02 Development board sample code. 
* @param void
* @return does not return  
*/
int main(void)
{			
    uint32_t i;

    
    board_init();                                   //initialize clk, timer0, uart, enable interrupts

    //At this point (1st time only) the USB-to-Serial port (ie. TeraTerm) can be found and setup. If power is not removed from CEC1702 Development 
    //   board then the USB-to-serial port will remain viable  The UART1 port is setup for 9600,8,1SB,NP         
    print_str("\nCEC1x02 Development Bd Blinking LED for MPLABX v1.2\n");

    print_str("\nObserve red LED (LD3) on CEC1x02 Development board \n");
    print_str("\n    blinking @ 500 msec interval...");
    
    int counter = 0;
    while(1)
	{
		timer_delay_1ms(500);
       	gpio_output_set (PIN_0156, GPIO_ALT_OUT_DIS, 0u);           //turn off LED3 on GPIO156
       	gpio_output_set (PIN_0157, GPIO_ALT_OUT_DIS, 1u);           //turn on LED4 on GPIO157
		timer_delay_1ms(500);
       	gpio_output_set (PIN_0156, GPIO_ALT_OUT_DIS, 1u);           //turn on LED3 on GPIO156
       	gpio_output_set (PIN_0157, GPIO_ALT_OUT_DIS, 0u);           //turn off LED3 on GPIO157
        if (counter == 0)
        {
            gpio_output_set (PIN_0027, GPIO_ALT_OUT_DIS, 0u);           //turn off LED-RGB on GPIO027 - Red
            gpio_output_set (PIN_0127, GPIO_ALT_OUT_DIS, 0u);           //turn off LED-RGB on GPIO127 - Green
            gpio_output_set (PIN_0162, GPIO_ALT_OUT_DIS, 0u);           //turn off LED-RGB on GPIO157 - Blue
        }
        else if (counter == 1)
        {
            gpio_output_set (PIN_0027, GPIO_ALT_OUT_DIS, 1u);           //turn on LED-RGB on GPIO027 - Red
            gpio_output_set (PIN_0127, GPIO_ALT_OUT_DIS, 0u);           //turn off LED-RGB on GPIO127 - Green
            gpio_output_set (PIN_0162, GPIO_ALT_OUT_DIS, 0u);           //turn off LED-RGB on GPIO157 - Blue
        }
        else if (counter == 2)
        {
            gpio_output_set (PIN_0027, GPIO_ALT_OUT_DIS, 0u);           //turn off LED-RGB on GPIO027 - Red
            gpio_output_set (PIN_0127, GPIO_ALT_OUT_DIS, 1u);           //turn on LED-RGB on GPIO127 - Green
            gpio_output_set (PIN_0162, GPIO_ALT_OUT_DIS, 0u);           //turn off LED-RGB on GPIO157 - Blue
        }
        else if (counter == 3)
        {
            gpio_output_set (PIN_0027, GPIO_ALT_OUT_DIS, 0u);           //turn off LED-RGB on GPIO027 - Red
            gpio_output_set (PIN_0127, GPIO_ALT_OUT_DIS, 0u);           //turn off LED-RGB on GPIO127 - Green
            gpio_output_set (PIN_0162, GPIO_ALT_OUT_DIS, 1u);           //turn on LED-RGB on GPIO157 - Blue
        }
        else if (counter == 4)
        {
            gpio_output_set (PIN_0027, GPIO_ALT_OUT_DIS, 1u);           //turn on LED-RGB on GPIO027 - Red
            gpio_output_set (PIN_0127, GPIO_ALT_OUT_DIS, 1u);           //turn on LED-RGB on GPIO127 - Green
            gpio_output_set (PIN_0162, GPIO_ALT_OUT_DIS, 0u);           //turn off LED-RGB on GPIO157 - Blue
        }
        else if (counter == 5)
        {
            gpio_output_set (PIN_0027, GPIO_ALT_OUT_DIS, 1u);           //turn on LED-RGB on GPIO027 - Red
            gpio_output_set (PIN_0127, GPIO_ALT_OUT_DIS, 0u);           //turn off LED-RGB on GPIO127 - Green
            gpio_output_set (PIN_0162, GPIO_ALT_OUT_DIS, 1u);           //turn on LED-RGB on GPIO157 - Blue
        }
        else if (counter == 6)
        {
            gpio_output_set (PIN_0027, GPIO_ALT_OUT_DIS, 0u);           //turn off LED-RGB on GPIO027 - Red
            gpio_output_set (PIN_0127, GPIO_ALT_OUT_DIS, 1u);           //turn on LED-RGB on GPIO127 - Green
            gpio_output_set (PIN_0162, GPIO_ALT_OUT_DIS, 1u);           //turn on LED-RGB on GPIO157 - Blue
        }
        else if (counter == 7)
        {
            gpio_output_set (PIN_0027, GPIO_ALT_OUT_DIS, 1u);           //turn on LED-RGB on GPIO027 - Red
            gpio_output_set (PIN_0127, GPIO_ALT_OUT_DIS, 1u);           //turn on LED-RGB on GPIO127 - Green
            gpio_output_set (PIN_0162, GPIO_ALT_OUT_DIS, 1u);           //turn on LED-RGB on GPIO157 - Blue
        }
        else
        {
            counter = -1;
        }
        counter++;
    }
      
}

/**  board_init
* @note	 Initialize CEC1702 clocks, timers, etc 
* @return void
*/
void board_init()
{
    uint32_t osc_loop = 0;

    
    //PCR_REGS->PCR_PROC_CLK_CNTRL =  PCR_PROC_CLK_CNTRL_PROCESSOR_CLOCK_DIVIDE_DIVIDE_BY_1_Val;  //use  48 MHz processor clock  
    PCR_REGS->PCR_PROC_CLK_CTRL =  PCR_PROC_CLK_CTRL_DIV_DIV_BY_1_Val;  //use  48 MHz processor clock  

    //VBAT_REGS->VBAT_CLOCK_EN = 0;                                   //there is no crystal, on the PIM, use internal OSC
    VBAT_REGS->VBAT_CLK32_EN = 0;

    //EC_REG_BANK_REGS->EC_REG_BANK_INTERRUPT_CONTROL &= ~1;          //clear the EC only interrupt control bit to use Agg NVIC
    EC_REG_BANK_REGS->EC_REG_BANK_INTR_CTRL &= ~1;          //clear the EC only interrupt control bit to use Agg NVIC
    timer_init();                                                   //Initialize 16-bit Timer1

    for (osc_loop = 0; osc_loop < 100000; osc_loop++)
    {
       if (PCR_REGS->PCR_OSC_ID == PCR_OSC_ID_PLL_LOCK(1))          //wait for OSC Lock
            break;
    }
    if (PCR_REGS->PCR_OSC_ID == PCR_OSC_ID_PLL_LOCK(0) )
    {
		while (1);	                                        //hang here to indicate OSC_LOCK failure
    } 
    
    //Initialize GPIO156 for LED3
    gpio_init(PIN_0027, GPIO_MUX_GPIO, GPIO_NON_INVERTED, GPIO_OUTPUT, GPIO_PUSH_PULL, GPIO_INTDET_DISABLED, GPIO_VTR, GPIO_PU);
	gpio_output_set (PIN_0027, GPIO_ALT_OUT_DIS, 0u);               //set low
    gpio_init(PIN_0127, GPIO_MUX_GPIO, GPIO_NON_INVERTED, GPIO_OUTPUT, GPIO_PUSH_PULL, GPIO_INTDET_DISABLED, GPIO_VTR, GPIO_PU);
	gpio_output_set (PIN_0127, GPIO_ALT_OUT_DIS, 0u);               //set low
    gpio_init(PIN_0162, GPIO_MUX_GPIO, GPIO_NON_INVERTED, GPIO_OUTPUT, GPIO_PUSH_PULL, GPIO_INTDET_DISABLED, GPIO_VTR, GPIO_PU);
	gpio_output_set (PIN_0162, GPIO_ALT_OUT_DIS, 0u);               //set low
    gpio_init(PIN_0156, GPIO_MUX_GPIO, GPIO_NON_INVERTED, GPIO_OUTPUT, GPIO_PUSH_PULL, GPIO_INTDET_DISABLED, GPIO_VTR, GPIO_PU);
	gpio_output_set (PIN_0156, GPIO_ALT_OUT_DIS, 0u);               //set low
    gpio_init(PIN_0157, GPIO_MUX_GPIO, GPIO_NON_INVERTED, GPIO_OUTPUT, GPIO_PUSH_PULL, GPIO_INTDET_DISABLED, GPIO_VTR, GPIO_PU);
	gpio_output_set (PIN_0157, GPIO_ALT_OUT_DIS, 0u);               //set low

    uart_init();                                                    //Initialize UART1 

	__enable_irq();                                                 //enable interrupts 
   
}

/**  timer_init
* @note	 Initialize 16-bit timer 1 to interrupt at 10 �sec intervals
* @return void
*/
void timer_init()
{
	unsigned short i;

    
    //TIMER16_0_REGS->TIMER16_CONTROL = 0x10;                        //soft reset 16-bit basic timer1
    TIMER16_0_REGS->TIMER16_CTRL = 0x10;                        //soft reset 16-bit basic timer1
     
 	interrupt_init(DIRECT_NVIC, MEC_GIRQ23_BITMASK);                //set up the NVIC for the timer interrupt
	interrupt_device_enable(BTMR0_IROUTE);

    //TIMER16_0_REGS->TIMER16_PRE_LOAD = 1000;                        //1 msec
    TIMER16_0_REGS->TIMER16_PRLD = 1000;                        //1 msec
    //TIMER16_0_REGS->TIMER16_INT_EN = 1;                             //enable interrupt for timer1
    TIMER16_0_REGS->TIMER16_IEN = 1;                             //enable interrupt for timer1
    //TIMER16_0_REGS->TIMER16_CONTROL = (0x29 | TIMER_PRE_SCL_DIV_48); //start auto count-down timer w/48MHz Pre-scale
    TIMER16_0_REGS->TIMER16_CTRL = (0x29 | TIMER_PRE_SCL_DIV_48); //start auto count-down timer w/48MHz Pre-scale

}


/**  TIMER16_0_Handler
* @note	 timer interrupt handler fires 1 msec intervals when using Direct NVIC mode
*/
void TIMER16_0_Handler()
{
	uint32_t interrupt_result;

	
	interrupt_result = p_interrupt_ecia_girq_result_get(MEC_GIRQ23_ID, GIRQ23_TMR0_BITPOS);   //check if bit 0 (timer 0 ) is the interrupt source
    
	if (interrupt_result)
	{
		p_interrupt_ecia_girq_source_clr(MEC_GIRQ23_ID, GIRQ23_TMR0_BITPOS);	//clear the interrupt source	

		ISR_COUNT++;
		if (ISR_COUNT >= TIMER0_TIMEOUT)							
		{

			ISR_COUNT = 0;
			T_DONE = 1;		
		}	    
	}		
}

/**  timer_delay_1ms
* @note	 clear timer irq flags and wait 1 msec before returning
* @param how_long number of milli-seconds to delay 
* @return void
*/
void timer_delay_1ms(uint32_t msec)				
{

	TIMER0_TIMEOUT = msec;										
	T_DONE = 0;
	ISR_COUNT = 0;
		
	while (!T_DONE);												//wait here for the timer to finish
}


/* End of CEC1x02_main.c*/
