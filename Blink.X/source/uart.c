/*****************************************************************************
* � 2016 Microchip Technology Inc. and its subsidiaries.
* You may use this software and any derivatives exclusively with
* Microchip products.
* THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".
* NO WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE,
* INCLUDING ANY IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY,
* AND FITNESS FOR A PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP
* PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.
* IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
* INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
* WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
* BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.
* TO THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL
* CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF
* FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
* MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE
* OF THESE TERMS.
*****************************************************************************/
/** @file  uart.c
 ******************************************************************************
*/
#include <xc.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "uart.h"


#define USE_UART_INTS


void uart_init(void);
void uart_output(void);
void print_str(char *);	
uint8_t get_key(void);
//void NVIC_Handler_GIRQ15(void);
#define TX_BYTE_CNT 400
char uart_tx_buff[TX_BYTE_CNT];
uint8_t UART_RCV_BUF;



void print_str(char *mychar)	//output <CR, LF>
{
	uint8_t ctr;
    uint8_t offset = 0;
    
      
	ctr = strlen(mychar);    
    
    ctr += offset;
    
	strcpy(&uart_tx_buff[offset],mychar);
	
    if (uart_tx_buff[ctr-1] == '\n')
        uart_tx_buff[ctr] = '\r';
	
    uart_tx_buff[ctr+1] = '\0';

	uart_output();

}

void uart_output()
{
	unsigned char cnt, i;
    
    
	//without interrupts
	cnt = strlen(uart_tx_buff);                                     //outputs string 1 byte at a time, get number of bytes in string
	for (i=0; i < cnt; )
	{
        while( 0 == p_uart_line_status_reg_get(UART1_ID, TRANSMIT_HOLDING_REG_EMPTY) );  //wait for Tx buffer empty 
        uart_transmit( UART1_ID, uart_tx_buff[i++] );               //output a character  
    }

}

/** uart_init
* @note	UART hardware initialization
* @return null
*/
void uart_init(void) {

     
    uart_pins_init( UART1_ID );                 //Initialize GPIO170 for TX

    uart_hw_init( UART1_ID, UART_CFG_SEL_POL_NON_INV, UART_CFG_SEL_PWR_VTR, UART_CLK_INT_1P84MHz, BAUD_9600, UART_FIFO_DIS, UART_FIFO_INT_LVL_1 );
    
    uart_protocol_init( UART1_ID, UART_WRD_LEN_8_BITS, UART_STOP_BIT_1, UART_PARITY_AS_EVEN, UART_INT_DISABLED );

}

/* last line of uart.c */
