Building SPI Image for CEC1x02_DevBd_BlinkLED.MPLABX
====================================================
From within CEC1x02_DevBd_BlinkLED.MPLABX.v1.2/spi_image run 'make_spi.bat' to generate: spi_image.bin
This binary 'spi_image.bin' is loadable to CEC1x02 SPI Flash.

The 'make_spi.bat' catch file calls 'postbuild.bat' and copies the resulting hex file from: 
	../dist/default/production/CEC1x02_DevBd_BlinkLED.MPLABX.v1.2.production.hex  to this directory.

The offset in postbuild.bat command line:
	'srec_cat %RESULTDIR%\%HEX_NAME% -intel -offset -0x0B0000 -o %RESULTDIR%\%ABS_NAME% -intel' 
must be the same as the origin of the project file. (In this case offset '0xB0000' is the link address.)

The files 'spi_flash_abs.hex' and 'spi_flash_abs.bin' are intermediary converted files which are used by:
    'mec2016_spi_gen.exe' and 'spi_cfg.txt' to generate the desired encoded 'spi_image.bin' which can 
be loaded by the user to MEC170x SPI flash using the proper external tools.   


NOTES:
======
Usage Doc for the SPI Image generator
=====================================
SPI Image Utility - 
    > mec2016_spi_gen.exe -i <cfg_file_name> -o <output_spi_file_name> -m <merge_file>

Running "mec2016_spi_gen.exe" from command prompt will take 
    "spi_cfg.txt" as a default configuration file and 
    generators the output "spi_image.bin"

Other options:
==============    
    -i cfg_file_name 
        Specifies the text config file for the SPI chip & images.
        Defaults to spi_cfg.txt

    -o output_spi_file_name
        Specifies the SPI binary output file name.
        Defaults to spi_image.bin

    -m merge_file
        Read merge file as an existing SPI binary image and create FW images 
        inside it.
        No default value

        
Configuration Details:
======================        
MEC2016 SPI Image Generator configuration file 
SPI Configuration
    [SPI]
    ;SPI Flash Image Size in Megabits 16 => 2MB
    SPISizeMegabits = 16

Image Details at Tag 0
    [IMAGE "0"]
    ;Image Vendor Identification string
    HeaderVendorID = MCHP
    
    ;Image Header Version
    HeaderVersion = <Any value >
    
    ;Image location in the SPI Flash
    ImageLocation = <Hex value>
    
    ;SPI read frequency supported 12 16 24 48 in Mhz
    SpiFreqMHz = select any frequency from above 
    
    ; SPI Read mode configuration supported "slow" or "fast" or "dual" or "quad"
    SpiReadCommand = slow / fast / dual / quad
    
    ; SPI pin drive strength: 2, 4, 8, or 12 mA
    SpiDriveStrength = Select Drive strength from above list
    
    ; SPI pin slew rate slow(false) or fast (true)
    SpiSlewFast = false / true
    
    ; SPI Signal Control values default is 00
    ; bit[0] SPI Clock Polarity, corresponds to QMSPI Mode register bit [8] 
    ;        1=SPI Clock starts High
    ;        0=SPI Clock starts Low
    ; bit[1] if SPI Clock Polarity Bit[0] above is 1 
    ;           1=Data changes on the falling edge of the SPI clock
    ;           0=Data changes on the rising edge of the SPI clock
    ;        if SPI Clock Polarity Bit[0] above is 0 
    ;           1=Data changes on the rising edge of the SPI clock
    ;           0=Data changes on the falling edge of the SPI clock
    ; bit[2] if SPI Clock Polarity Bit[0] above is 1 
    ;           1=Data are captured on the rising edge of the SPI clock
    ;           0=Data are captured on the falling edge of the SPI clock
    ;        if SPI Clock Polarity Bit[0] above is 0
    ;           1=Data are captured on the falling edge of the SPI clock
    ;           0=Data are captured on the rising edge of the SPI clock
    SpiSignalControl = 0x00
    
    ; These header VTRx Level Bit values will take precedence and overwrite 
    ; any previous programmed VTRx_Levels.  If not enabled, no change to the current VTRx bit value. 
    ; By default VTRx values will be 3.3V  or false
    VTR1pinSrc18 = false
    VTR2pinSrc18 = false
    VTR3pinSrc18 = false

    ; Enable Authentication of Header, FW 
    ; Generate ECDSA signature of 64-byte Header, padded FW binary + optional encryption key header,
    ; If false bytes[31:0] of the signatures contain the SHA256(object)
    UseECDSA = false / true
    
    ; This EC key pair is used to sign and verify/authenticate the FW Image Header, 
    ; FW + optional key header optional key header.
    ; EC Private Key in PEM encoded Openssl SSLeay encrypted format
    ; This key is used to sign the Header and is NOT stored in the MEC chip.
    ECDSAPrivKeyFile = Authentication Key.pem 
    ECDSAPrivKeyPassword = PASSWORD for the Private Key

    ;To Encrypt Application binary using AES-256-CBC
    FwEncrypt = false / true
    
    ; FW may be AES-256-CBC encrypted
    ; The key is auto-generated and exchanged with the ROM using a procedure 
    ; based on ECDH.
    ; An EC Public Key is used by this program to Generate the AES-256 Key/IV 
    ; and a 64-byte key header appended to the encrypted FW binary. 
    ; The corresponding EC Private key is stored in the MEC chip and is used by 
    ; ROM to re-generated the AES-256 Key & IV.
    AesGenECPubKeyFile = <Encryption Certificate file >
    
    ;Firmware Application binary image
    FwBinFile = <Application Binary file . bin>
    
    ; Offset of 0 means append to end of header. Non-zero value locates FW at ImageLocation + FWOffset
    FwOffset = 0 
    
    ;Application firmware load address in SRAM
    FwLoadAddress = <Load address in Hex>
    
    ;Zero means get entry address from offset 0x4 of input Application binary which is the reset handler address
    ;If FWEntryAddress is non-zero then use it as entry point.
    FwEntryAddress = 0 or <Entry address in Hex if known>
    