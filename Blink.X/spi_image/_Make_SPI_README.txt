To make a SPI Flash binary image for programming to CEC1x02 SPI Flash  :
	In this directory, double-click on : 'make_spi.bat' 
	This will result in : 'spi_image.bin' (a programmable SPI Flash image)	

'make_spi.bat' converts '..\dist\default\production\CEC1x02_DevBd_BlinkLED_MPLABX.v1.2.production.hex' from MPLABX IDE 
	build process to binary for SPI flash programming.
	

To modify the project name, modify file : 'make_spi.bat' 
			postbuild.bat 'projectname'

To modify the pathname for the input .hex file modify 'postbuid.bat' file