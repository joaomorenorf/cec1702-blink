@ECHO OFF
ECHO -------------------------------------------------------------------------
ECHO              P O S T - B U I L D   O P E R A T I O N            
ECHO -------------------------------------------------------------------------

SET PRJNAME=%1


SET HEX_NAME=%PRJNAME%.production.hex
SET ABS_NAME=spi_flash_abs.hex
SET BIN_NAME=spi_flash_abs.bin
SET RESULTDIR=.

SET SRC_PATH=..\dist\default\production

IF  EXIST %SRC_PATH%\%HEX_NAME%    COPY %SRC_PATH%\%HEX_NAME%   %RESULTDIR% /y 

@ECHO ... Relocate HEX to offset 0
srec_cat %RESULTDIR%\%HEX_NAME% -intel -offset -0x0B0000 -o %RESULTDIR%\%ABS_NAME% -intel

@ECHO ... Create binary from Absolute(zero offset) HEX
srec_cat %RESULTDIR%\%ABS_NAME% -intel -o %RESULTDIR%\%BIN_NAME% -binary
ECHO -------------------------------------------------------------------------

@ECHO OFF
mec2016_spi_gen_64_bit spi_cfg.txt

::del %ABS_NAME%
::del %BIN_NAME%
::del %HEX_NAME%
::del MSG
@ECHO ON
