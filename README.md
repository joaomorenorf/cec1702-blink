# Blink Sketch for CEC1702

This is a simple blink sketch for CEC1702. I had a lot trouble trying to make CEC work and I didn't find any code on the Internet. I've contacted Microchip Support and they sent me a zip folder called `CEC1x02_DevBd_BlinkLED.MPLABX.v1.2.zip`. Unfortunately It didn't work so I started digging and this is the version it worked for me, I hope this will help someone else.

You will see that I changed some register names to be able to use updated libraries and I also made the code a little more complex. This board has 2 simple LEDs (IO156 - LD3 and IO157 - LD4) and a RGB LED (IO027 - Red, IO127 - Green and IO162 - Blue). This demo code will blink all these LEDs and will also output an initial message at USB tty.

## Video demonstration

[https://www.youtube.com/embed/EvJsJSYToPA](https://www.youtube.com/embed/EvJsJSYToPA)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/EvJsJSYToPA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## My setup
* CEC1702PIM
* CEC1X02 Development Board - DM990013
* PICKit4 - PG164140
* MPLABX v5.50
* XC32 v3.01
* CMSIS 5.4.0
* CEC_DFP 1.3.106
* Manjaro 5.13.5

## PICkit 4 Pinout

To connect PICkit4 to the Dev board I used JTAG/SWD 20 pin connector following the pinout of [AC102015 Debugger Adapter Board](https://www.microchip.com/en-us/development-tool/AC102015). The pictures below will guide you through the whole connection.

![PICkit4 Schematics pinout](images/PICkit4-pinout.png)
![Dev Board Schematics pinout](images/DevBoard-pinout.png)
![PICkit4 Connection Example](images/PICkit4-connection-example.jpeg)
![Dev Board Connection Example](images/DevBoard-connection-example.jpeg)
